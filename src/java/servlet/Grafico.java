package servlet;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import twitter4j.TwitterException;
import twitterText.Analisis;
import twitterText.TwitterAPI;

/**
 *
 * @author gera88t
 */
@WebServlet(urlPatterns = {"/grafico"})
public class Grafico extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
      
        
       
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
         
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String consulta = request.getParameter("consulta");
        String fechaDesde = request.getParameter("fechaDesde");
        String fechasHasta= request.getParameter("fechaHasta");
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
         TwitterAPI ta = null;
         int pn[]= new int[2];
         
        try {
            ta = new TwitterAPI();
        } catch (TwitterException ex) {
            Logger.getLogger(Grafico.class.getName()).log(Level.SEVERE, null, ex);
        }
        Analisis a = new Analisis();
        try {
            if(fechaDesde==null){
                fechaDesde="";
            }
            if(fechasHasta==null){
                fechasHasta="";
            }
            pn = a.analisis(ta.buscar(consulta,fechaDesde, fechasHasta));
            System.out.println(pn);
        } catch (TwitterException ex) {
            Logger.getLogger(Grafico.class.getName()).log(Level.SEVERE, null, ex);
        }
        //request.setAttribute("data", "["+ pn[0]+","+pn[1]+"]");
        //request.getRequestDispatcher("index.jsp").forward(request, response);
        response.getWriter().print("["+ pn[0]+","+pn[1]+"]");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
