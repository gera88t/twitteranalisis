/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package twitterText;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author gera88t
 */
public class Analisis {

    ArrayList<String> positivas = new ArrayList<String>();
    ArrayList<String> negativas = new ArrayList<String>();

    public Analisis() throws FileNotFoundException, IOException {

        String linea = "";

        FileReader fp = new FileReader("/home/gera88t/NetBeansProjects/SentimientosTwitter/src/java/twitterText/pspa.txt");
        this.positivas = this.llenar(fp);

        FileReader fn = new FileReader("/home/gera88t/NetBeansProjects/SentimientosTwitter/src/java/twitterText/nspa.txt");
        this.negativas = this.llenar(fn);

    }

    private ArrayList<String> llenar(FileReader fn) throws IOException {
        ArrayList<String> lista = new ArrayList<>();
        BufferedReader b = new BufferedReader(fn);
        String linea = "";
        while ((linea = b.readLine()) != null) {
            lista.add(linea);

        }
        b.close();
        return lista;
    }

    public int[] analisis(ArrayList<String> twitters) {
        int positivas = 0;
        int negativas = 0;

        for (int i = 0; i < twitters.size(); i++) {
            for (int j = 0; j < this.positivas.size(); j++) {
                if (twitters.get(i).contains(this.positivas.get(j))) {
                    positivas = positivas + 1;
                }

            }
            for (int k = 0; k < this.negativas.size(); k++) {
                if (twitters.get(i).contains(this.negativas.get(k))) {
                    negativas = negativas + 1;
                }
            }
        }
        System.out.print("positivas: " + positivas + " negativas: " + negativas);
        int pn[]= new int[2];
        pn[0]=positivas;
        pn[1]=negativas;
        return  pn;
    }

}
