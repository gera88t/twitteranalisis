<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Grafico</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    </head>
    <body>
        <div class="container">
            <div class="row">
                <form action="grafico" method="post" id="grafico">
                    <b>Texto a buscar</b>
                    <input type="text" name="consulta" required>
                    <b>Desde Fecha</b>
                    <input type="date" name="fechaDesde"required>
                    <b>Hasta Fecha</b>
                    <input type="date" name="fechaHasta" required>
                    <input type="submit">

                </form>
            </div>
            <div class="row">
                <canvas id="myChart" width="400" height="400"></canvas>
            </div>

        </div>
    </body>
    <script
        src="http://code.jquery.com/jquery-3.1.1.js"
        integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA="
    crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0/Chart.bundle.js"></script>
   
    <script>
        //ctx.clearRect(0,0,myChart.width, myChart.height);
        var dataChart = [];
        var ctx = document.getElementById("myChart");
        var myChart;
        
        $("#grafico").submit(function (event) {
            event.preventDefault();
            dataChart = [];
            $.post('grafico', $("#grafico").serialize()).done(function (data) {
                dataChart = data;
                myChart = new Chart(ctx, {
                    type: 'polarArea',
                    data: {
                        labels: ["Positivo", "Negativo"],
                        datasets: [{
                                label: '# de Palabras',
                                data: dataChart,
                                backgroundColor: [
                                    'rgba(54, 162, 235, 0.2)',
                                    'rgba(255, 99, 132, 0.2)'
                                ],
                                borderColor: [
                                    'rgba(54, 162, 235, 1)',
                                    'rgba(255,99,132,1)'
                                ],
                                borderWidth: 1
                            }]
                    }
//                            options: {
//                                scales: {
//                                    yAxes: [{
//                                            ticks: {
//                                                beginAtZero: true
//                                            }
//                                        }]
//                                }
//                            }
                });
            });
        });

    </script>
</html>
