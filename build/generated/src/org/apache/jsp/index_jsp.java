package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>JSP Page</title>\n");
      out.write("        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">\n");
      out.write("\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <div class=\"container\">\n");
      out.write("            <div class=\"row\">\n");
      out.write("                <form action=\"grafico\" method=\"post\" id=\"grafico\">\n");
      out.write("                    <input type=\"text\" name=\"consulta\" required>\n");
      out.write("                    <input type=\"submit\">\n");
      out.write("                </form>\n");
      out.write("            </div>\n");
      out.write("            <div class=\"row\">\n");
      out.write("                <canvas id=\"myChart\" width=\"400\" height=\"400\"></canvas>\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("        </div>\n");
      out.write("    </body>\n");
      out.write("    <script\n");
      out.write("        src=\"http://code.jquery.com/jquery-3.1.1.js\"\n");
      out.write("        integrity=\"sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA=\"\n");
      out.write("    crossorigin=\"anonymous\"></script>\n");
      out.write("    <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\" integrity=\"sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa\" crossorigin=\"anonymous\"></script>\n");
      out.write("    <script type=\"text/javascript\" src=\"https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0/Chart.bundle.js\"></script>\n");
      out.write("    <script>\n");
      out.write("        var dataChart = [];\n");
      out.write("        var ctx = document.getElementById(\"myChart\");\n");
      out.write("        $(\"#grafico\").submit(function (event) {\n");
      out.write("            event.preventDefault();\n");
      out.write("            $.post('grafico', function (data) {\n");
      out.write("                dataChart = data;\n");
      out.write("                var myChart = new Chart(ctx, {\n");
      out.write("                    type: 'bar',\n");
      out.write("                    data: {\n");
      out.write("                        labels: [\"Red\", \"Blue\"],\n");
      out.write("                        datasets: [{\n");
      out.write("                                label: '# of Votes',\n");
      out.write("                                data: dataChart,\n");
      out.write("                                backgroundColor: [\n");
      out.write("                                    'rgba(255, 99, 132, 0.2)',\n");
      out.write("                                    'rgba(54, 162, 235, 0.2)'\n");
      out.write("                                ],\n");
      out.write("                                borderColor: [\n");
      out.write("                                    'rgba(255,99,132,1)',\n");
      out.write("                                    'rgba(54, 162, 235, 1)'\n");
      out.write("                                ],\n");
      out.write("                                borderWidth: 1\n");
      out.write("                            }]\n");
      out.write("                    },\n");
      out.write("                    options: {\n");
      out.write("                        scales: {\n");
      out.write("                            yAxes: [{\n");
      out.write("                                    ticks: {\n");
      out.write("                                        beginAtZero: true\n");
      out.write("                                    }\n");
      out.write("                                }]\n");
      out.write("                        }\n");
      out.write("                    }\n");
      out.write("                });\n");
      out.write("            });\n");
      out.write("        });\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("    </script>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
